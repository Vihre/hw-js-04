let numFirst;
while(numFirst === null || numFirst === "" || isNaN(numFirst)){
    numFirst=+prompt("Add the first number").trim();
};
 
let numSecond;
while(numSecond === null || numSecond === "" || isNaN(numSecond)){
    numSecond=+prompt("Add the second number").trim();
};

let operator=prompt("Add a function +,-,*,/,<,>");


function calcResult(numFirst, numSecond, operator) {
    switch (operator) {
      case '+':
        return numFirst + numSecond;
      case '-':
        return numFirst - numSecond;
      case '*':
        return numFirst * numSecond;
      case '/':
        return numFirst / numSecond;
      case '<':
        return numFirst < numSecond;
      case '>':
        return numFirst > numSecond;
    }
  }

console.log(calcResult(numFirst, numSecond, operator));